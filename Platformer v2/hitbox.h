#pragma once

class HitBox {
public:
	float x, y, w, h;
	bool Overlaps(const HitBox&);
	bool IsReal(void);
	friend HitBox operator+ (const HitBox&, const HitBox&);
};

HitBox operator+ (const HitBox&, const HitBox&);

HitBox operator- (const HitBox&, const HitBox&);
