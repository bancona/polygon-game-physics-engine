#pragma once

#include "actor.h"

class Platform : public Actor {
public:
	Platform(float, float, float, float, float, float, Color);
};