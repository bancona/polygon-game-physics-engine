#pragma once

#include "stdafx.h"
#include "color.h"
#include "HitBox.h"

class Actor {
protected:
	bool alive_;
	Color color_;
	float x_speed_, y_speed_;
	HitBox hitbox_;
public:
	static std::vector<Actor*> actors;
	static const float k_grav_accel;
	static const float k_term_vel;
	Actor(HitBox hb, float xsp, float ysp, Color c);
	Actor(float x, float y, float w, float h, float xsp, float ysp, Color c);
	HitBox hitbox(void);
	float x_speed(void);
	float y_speed(void);
	bool alive(void);
	Color color(void);
	virtual void Paint(void);
	virtual void Act(bool keyStates[], bool keySpecialStates[]);
	virtual void Move(bool keyStates[], bool keySpecialStates[]);
	std::vector<Actor*> FindCollisions();
	void Actor::HandleCollisions(std::vector<Actor*>);
};

template <typename T> inline int signum(T val) {
	return (T(0) < val) - (val < T(0));
}