#include "stdafx.h"
#include "actor.h"

const float Actor::k_grav_accel = -0.002f;

const float Actor::k_term_vel = -0.03f;

std::vector<Actor*> Actor::actors;

Actor::Actor(float x, float y, float w, float h, float xsp, float ysp, Color c) {
	hitbox_ = HitBox{ x, y, w, h };
	x_speed_ = xsp;
	y_speed_ = ysp;
	color_ = c;
	alive_ = true;
	Actor::actors.push_back(this);
}

HitBox Actor::hitbox() {
	return hitbox_;
}

float Actor::x_speed(void) { return x_speed_; }

float Actor::y_speed(void) { return y_speed_; }

bool Actor::alive(void) { return alive_; }

void Actor::Paint(void) {
	glBegin(GL_QUADS);
	glColor3f(color_.r, color_.g, color_.b);
	glVertex2f(hitbox_.x, hitbox_.y);
	glVertex2f(hitbox_.x + hitbox_.w, hitbox_.y);
	glVertex2f(hitbox_.x + hitbox_.w, hitbox_.y + hitbox_.h);
	glVertex2f(hitbox_.x, hitbox_.y + hitbox_.h);
	glEnd();
}

void Actor::Act(bool keyStates[], bool keySpecialStates[]) {
	//Move(keyStates, keySpecialStates);
	//std::vector<Actor*> collisions = FindCollisions();
	//HandleCollisions(collisions);
}

void Actor::Move(bool keyStates[], bool keySpecialStates[]) {}

std::vector<Actor*> Actor::FindCollisions() {
	std::vector<Actor*> collisions;
	for (long i = 0; i < actors.size(); ++i) {
		Actor* curActor = actors.at(i);
		if (curActor == NULL || curActor == this
			|| !hitbox_.Overlaps(curActor->hitbox_)) continue;
		collisions.push_back(curActor);
	}
	return collisions;
}

void Actor::HandleCollisions(std::vector<Actor*> collisions) {
	if (collisions.size() == 0) return;
	
}

template <typename T> inline int signum(T val);