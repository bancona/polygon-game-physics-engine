#include "stdafx.h"
#include "hero.h"

Hero::Hero(float a, float b, float c, float d, float e, float f, Color g)
: Actor(a, b, c, d, e, f, g) {
	max_x_speed_ = .028f;
	max_y_speed_ = .03f;
	acceleration_ = .004f;
	deceleration_ = .006f;
	jump_speed_ = .045f;
	can_jump_ = true;
}

void Hero::Act(bool keyStates[], bool keySpecialStates[]) {
	Move(keyStates, keySpecialStates);
}

void Hero::Move(bool keyStates[], bool keySpecialStates[]) {
	if (keySpecialStates[GLUT_KEY_RIGHT]
		&& !keySpecialStates[GLUT_KEY_LEFT]) {
		// only right button pressed
		x_speed_ += acceleration_;
	}
	else if (keySpecialStates[GLUT_KEY_LEFT]
		&& !keySpecialStates[GLUT_KEY_RIGHT]) {
		// only left button pressed
		x_speed_ -= acceleration_;
	}
	else {
		Decelerate();
	}
	if ((keySpecialStates[GLUT_KEY_RIGHT] && x_speed_ < 0)
		|| (keySpecialStates[GLUT_KEY_LEFT] && x_speed_ > 0)) {
		// if directional button moving against current
		// direction, decelerate just as if no button
		// were pressed.
		Decelerate();
	}

	// lower abs value of x_speed to maximum speed if over
	if (std::fabs(x_speed_) > max_x_speed_) {
		x_speed_ = max_x_speed_ * signum(x_speed_);
	}

	if (x_speed_ != 0) {
		// move player by current speed
		hitbox_.x += x_speed_;

		// if collision, remove collision and set x_speed_ to 0
		std::vector<Actor*> collisions = FindCollisions();
		if (collisions.size() > 0) {
			hitbox_.x -= x_speed_;
			x_speed_ = 0;
		}
	}
	y_speed_ += k_grav_accel;

	if (y_speed_ < k_term_vel) y_speed_ = k_term_vel;

	if (keySpecialStates[GLUT_KEY_UP] && can_jump_) {
		y_speed_ += jump_speed_;
		can_jump_ = false;
	}

	hitbox_.y += y_speed_;

	std::vector<Actor*> collisions = FindCollisions();
	if (collisions.size() > 0) {
		hitbox_.y -= y_speed_;
		if (y_speed_ < 0) can_jump_ = true;
		y_speed_ = 0;
	}
	else can_jump_ = false;
}

void Hero::Decelerate() {
	int orig_signum = signum(x_speed_);
	x_speed_ -= orig_signum * deceleration_;
	if (orig_signum != signum(x_speed_))
		x_speed_ = 0;
}

bool Hero::can_jump(void) { return can_jump_; }

float Hero::jump_speed(void) { return jump_speed_; }

float Hero::jump_float(void) { return jump_float_; }

float Hero::max_x_speed(void) { return max_x_speed_; }

float Hero::max_y_speed(void) { return max_y_speed_; }

float Hero::acceleration(void) { return acceleration_; }

float Hero::deceleration(void) { return deceleration_; }