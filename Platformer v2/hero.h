#pragma once

#include "actor.h"

class Hero : public Actor {
protected:
	bool can_jump_;
	float max_x_speed_, max_y_speed_;
	float acceleration_, deceleration_;
	float jump_speed_, jump_float_;
public:
	Hero(float x, float y, float w, float h, float xspd, float yspd, Color c);
	bool can_jump(void);
	float max_x_speed(void);
	float max_y_speed(void);
	float acceleration(void);
	float deceleration(void);
	float jump_speed(void);
	float jump_float(void);
	void Act(bool keyStates[], bool keySpecialStates[]);
	void Move(bool keyStates[], bool keySpecialStates[]);
	void Decelerate();
};