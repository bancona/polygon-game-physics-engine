#include "stdafx.h"
#include "HitBox.h"

HitBox operator+ (const HitBox& box1, const HitBox& box2) {
	float l = std::fmin(box1.x, box2.x);
	float r = std::fmax(box1.x + box1.w, box2.x + box2.w);
	float b = std::fmin(box1.y, box2.y);
	float t = std::fmax(box1.y + box1.h, box2.y + box2.h);
	return HitBox{ l, b, r - l, t - b };
}

HitBox operator- (const HitBox& box1, const HitBox& box2) {
	float l = std::fmax(box1.x, box2.x);
	float r = std::fmin(box1.x + box1.w, box2.x + box2.w);
	float b = std::fmax(box1.y, box2.y);
	float t = std::fmin(box1.y + box1.h, box2.y + box2.h);
	return HitBox{ l, b, r - l, t - b };
}

bool HitBox::Overlaps(const HitBox& box) {
	return !(x + w < box.x
		|| box.x + box.w < x
		|| y + h < box.y
		|| box.y + box.h < y);
}

bool HitBox::IsReal() {
	return w > 0 && h > 0;
}