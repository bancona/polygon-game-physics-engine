#include "stdafx.h"
#include "hero.h"
#include "platform.h"
#include <chrono>

int interval = 1000 / 60; //60 fps
bool keyStates[256];
bool keySpecialStates[256];


void update(int value) {
	printf("%d\n", std::chrono::system_clock::now().time_since_epoch().count()/10000);

	//Call update() again in 'interval' milliseconds
	glutTimerFunc(interval, &update, 0);

	for (long val = 0; val < Actor::actors.size(); val++) {
		Actor::actors.at(val)->Act(keyStates, keySpecialStates);
	}

	//Redisplay frame
	glutPostRedisplay();
}

void keyPressed(unsigned char key, int x, int y) {
	keyStates[key] = true;
}

void keyUp(unsigned char key, int x, int y) {
	keyStates[key] = false;
}

void keySpecialPressed(int key, int x, int y) {
	keySpecialStates[key] = true;
}

void keySpecialUp(int key, int x, int y) {
	keySpecialStates[key] = false;
}

void display(void);

void init(void) {
	//Configure basic OpenGL settings
	glClearColor(0.0, 0.0, 0.0, 1.0);
	glShadeModel(GL_SMOOTH);
	glEnable(GL_BLEND);
	glEnable(GL_TEXTURE_2D);

	// Register callback functions
	glutTimerFunc(interval, &update, 0);
	glutKeyboardFunc(keyPressed);
	glutKeyboardUpFunc(keyUp);
	glutSpecialFunc(keySpecialPressed);
	glutSpecialUpFunc(keySpecialUp);
	glutDisplayFunc(&display);
}

Hero* hero_p;

int main(int argc, char** argv) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(800, 600);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("Bertie's Platformer");

	init();

	hero_p = new Hero(.0f, .0f, .1f, .2f, .025f, .02f,
		Color{ .7f, .1f, .1f });
	new Platform(.3f, -.3f, .5f, .1f, 0, 0,
		Color{ .1f, .7f, .1f });
	new Platform(-1, -1, 2, .1f, 0, 0,
		Color{ .1f, .7f, .1f });
	new Platform(0, -1, .1f, .4f, 0, 0,
		Color{ .1f, .7f, .1f });
	new Platform(.9f, -.3f, .5f, .5f, 0, 0,
		Color{ .4f, 0, .4f });
	new Platform(1.7f, -.3f, .5f, .5f, .0, 0,
		Color{ .8f, 0, .4f });


	glutMainLoop();

	return 0;
}

void display(void) {
	//Clear the screen and set our initial view matrix
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// move camera to center on hero
	glTranslatef(-hero_p->hitbox().x + hero_p->hitbox().w / 2.f,
		-hero_p->hitbox().y - hero_p->hitbox().h / 2.f, 0);

	glTranslatef(-.1f, -.1f, 0);

	//Draw all actors to screem
	for (long val = 0; val < Actor::actors.size(); val++) {
		Actor::actors.at(val)->Paint();
	}


	//Need to switch to the front buffer to show it on screen.
	glutSwapBuffers();
}
